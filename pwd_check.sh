if [ $PWD != $HOME ] ; then
	echo "You must be in HOME directory (~) to run this shell script"
else
	echo "You are in the correct directory to run this shell script"
fi
