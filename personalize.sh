#!/bin/bash
if [ $PWD != $HOME ] ; then
	echo "You must be in HOME directory (~) to run this shell script"
else
	git clone https://jborreloo@bitbucket.org/jborreloo/bashrc.git
	cd ~
	git clone https://jborreloo@bitbucket.org/jborreloo/vimrc.git
	cd ~
	git clone https://jborreloo@bitbucket.org/jborreloo/bitrepomaker.git
	cd ~
	git clone https://jborreloo@bitbucket.org/jborreloo/tmux.git
	cd ~
	apt install tmux -y
	cd ~
	apt install tightvncserver -y
	cd ~
	apt install bzip2 -y
	cd ~
	apt install wget -y
	cd ~
	apt install vim -y
	cd ~
	git clone https://github.com/gpakosz/.tmux.git
	ln -s -f .tmux/.tmux.conf
	cp .tmux/.tmux.conf.local .
	cd ~
	cat ~/bashrc/.bashrc >> ~/.bashrc
	chmod +x ~/vimrc/vundle_install.sh
	./vimrc/vundle_install.sh
	cp ~/vimrc/.vimrc ~/.vimrc
	cp ~/bitrepomaker/repomake.sh ~/repomake.sh
	chmod +x ~/repomake.sh
	wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh	
	chmod +x ~/Anaconda3-2021.05-Linux-x86_64.sh
	.Anaconda3-2021.05-Linux-x86_64.sh
	cd ~
	export PATH=~/anaconda3/bin:$PATH
	cd ~
	git clone https://jborreloo@bitbucket.org/jborreloo/clean-and-update.git
	chmod +x ~/clean-and-update/clean-and-update.sh
	sh ~/clean-and-update/clean-and-update.sh
fi
